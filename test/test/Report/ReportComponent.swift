//
//  ReportComponent.swift
//  test
//
//  The component tracks the lifecycle of objects instantiated in module
//  this is usefull in the case where we could have shared instances, between or accross features
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

class ReportComponent {
    
    private static var reportModule: ReportModule?
    
    func inject(reportView: ReportViewController) {
        ReportComponent.reportModule = ReportModule(reportView: reportView)
        if let reportModule = ReportComponent.reportModule {
            reportView.reportPresenter = reportModule.provideReportPresenter()
            reportView.reportTableViewAdapter = reportModule.provideReportAdapter()
        }
    }
}
