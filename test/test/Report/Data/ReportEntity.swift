//
//  ReportEntity.swift
//  test
//
//  This class holds the data loaded from the services
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

import Foundation

class ReportEntity {
    let batteryLevel: Double
    let charging: Bool
    let time: String
    let location: Location?
    
    init(batteryLevel: Double, charging: Bool, time: String, location: Location? = nil) {
        self.batteryLevel = batteryLevel
        self.charging = charging
        self.time = time
        self.location = location
    }
}
