//
//  ReportDelegate.swift
//  test
//
//  The delegate is in charge of loading data using ApiServices
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

protocol ReportDelegate {
    func loadReports(index: Int, count: Int, onResponse: ([ReportEntity]) -> Void, onError: (Error) -> Void)
    func loadLocation(with Id: String)
}
