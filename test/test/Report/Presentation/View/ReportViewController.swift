//
//  ReportViewController.swift
//  test
//
//  Link your storyboard to this viewController
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//


import UIKit

class ReportViewController: UIViewController, ReportView
{
    let reportComponent = ReportComponent()
    // MARK: - Injected
    var reportPresenter: ReportPresenter?
    var reportTableViewAdapter: ReportTableViewAdapter?
    
    @IBOutlet weak var reportTableView: UITableView!
    
    // MARK: - View lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        reportComponent.inject(reportView: self)
        reportTableView.delegate = reportTableViewAdapter
        reportTableView.dataSource = reportTableViewAdapter
        reportPresenter?.loadData()
    }
    
    // MARK: - Display logic
    func displayMessage(message : String) {
        showDialog(title: "Success!", message: message)
    }
    
    func showErrorMessage(errorMessage : String) {
        showDialog(message: errorMessage)
        print("Some Error occured: \(errorMessage)")
    }
    
    func showLoading() {
        print("Something is loading, show the spinner")
    }

    func hideLoading() {
        print("Something finished loading, hide the spinner")
        reportTableView.reloadData()
    }
    
    private func showDialog(title: String = "", message: String = "") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
