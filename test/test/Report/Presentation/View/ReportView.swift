//
//  ReportView.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//
//

protocol ReportView {
    var reportPresenter: ReportPresenter? { get set }

    func displayMessage(message : String)
    func showErrorMessage(errorMessage : String)
    func showLoading()
    func hideLoading()
}
