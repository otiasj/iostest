//
//  ReportCell.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright © 2019 coinspect. All rights reserved.
//

import Foundation
import UIKit

class ReportCell: UITableViewCell {
    
    private var presenter: ReportPresenter?
    
    @IBOutlet weak var chargingIcon: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    
    @IBAction func onLocationClicked(_ sender: Any) {
//        presenter?.loadLocation(with: )
    }
    
    func bind(with report: ReportEntity, presenter: ReportPresenter) {
        dateLabel.text = "Report Date: \(report.time)"
        batteryLabel.text = "Battery Level: \(report.batteryLevel)%"

        if (report.charging) {
            
        } else {
            
        }
        
        if let _ = report.location {
            
        }

    }
}
